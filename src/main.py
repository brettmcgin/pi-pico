from machine import Pin, PWM
import uasyncio


class RgbLed:
    red: PWM
    green: PWM
    blue: PWM

    def __init__(self, red: PWM, green: PWM, blue: PWM):
        self.red = red
        self.green = green
        self.blue = blue


async def loop(led: RgbLed, sleepInMillis: int, initialFreqency=0):
    frequency = initialFreqency
    while True:
        print(f'frequency: {frequency} duration: {sleepInMillis}')
        led.red.duty_u16(frequency)
        led.green.duty_u16(frequency)
        led.blue.duty_u16(frequency)

        frequency += 1000
        if frequency > 20000:
            frequency = 0
        await uasyncio.sleep_ms(sleepInMillis)


async def blink(led: Pin, sleepInMillis: int, startOn: bool = True):
    state = startOn
    while True:
        print(f'led: {led}, state: {state}, duration: {sleepInMillis}')
        led.value(state)
        state = not state

        await uasyncio.sleep_ms(sleepInMillis)


async def main():
    led = RgbLed(
        red=PWM(Pin(13, Pin.OUT)),
        green=PWM(Pin(12, Pin.OUT)),
        blue=PWM(Pin(11, Pin.OUT))
    )

    dumbLedRed = Pin(15, Pin.OUT)
    dumbLedGreen = Pin(14, Pin.OUT)

    tasks = (
        loop(led, 100),
        blink(dumbLedRed, 200),
        blink(dumbLedGreen, 50)
    )

    await uasyncio.gather(*tasks)

uasyncio.run(main())

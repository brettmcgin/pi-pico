# Pi Pico

## Pin Reference

https://docs.micropython.org/en/latest/_images/pico_pinout.png

## Dev Board Prep

1. [Download latest uf2](https://micropython.org/download/rp2-pico/)
1. Hold down bootsel, while plugging in pi pico
1. Drag uf2 file to RPI-RP2

## Dev Env 

1. `python -m venv .env`
1. `source .env/bin/activate`

## Dev libs

install libs

`pip3 install -r requirements.txt`


Update libs

`pip3 freeze > requirements.txt`

## Dev run file with ampy

Note: you can use other tools for running on device Thonny / Pico-go / pymakr / micropython ide

`.env/bin/ampy -p /dev/tty.usbmodem2101 run main.py`

## Dev Vscode Intellisense

1. Install Pico micropython stub: https://github.com/cpwood/Pico-Stub/blob/main/micropy.md#using-the-stubs
1. micropy init